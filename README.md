# [Android] Bitbucket Viewer
An Android client to view user's public and private repositories, source code, and commits on [Bitbucket](https://bitbucket.org). It utilizes OAuth2 protocol for authentication. The app has no access to user's credential data.

![Bitbucket Viewer screen](img/screen1.jpg)
![Bitbucket Viewer screen](img/screen2.jpg)
![Bitbucket Viewer screen](img/screen3.jpg)
![Bitbucket Viewer screen](img/screen4.jpg)
![Bitbucket Viewer screen](img/screen5.jpg)
![Bitbucket Viewer screen](img/screen6.jpg)

## Libraries and frameworks:
- Retrofit2
- Dagger2
- Glide
- ButterKnife
- JUnit
- Robolectric
- Mockito
- Espresso

## Disclaimer
The app, as well as the author, are not affiliated with [Atlassian](https://www.atlassian.com/) company.

You can use the software for educational purposes only. The information
regarding the origin of the software shall be preserved.

The software is provided "as is", and without warranty of any kind. In 
no event shall the author be liable for any claim, tort, damage, or any
other liability.

By using the program, you agree to the above terms and conditions.
