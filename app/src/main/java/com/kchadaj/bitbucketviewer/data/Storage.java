package com.kchadaj.bitbucketviewer.data;

import android.content.SharedPreferences;

import javax.inject.Inject;

public class Storage {

    public static String ACCESS_TOKEN = "access_token";
    public static String FILES_COMMIT_HASH = "files_commit_hash";
    public static String FILES_PATH = "files_path";
    public static String IS_AUTH_URI_EXPECTED = "auth_uri";
    public static String REPO_SLUG = "repo_slug";
    public static String USERNAME = "username";

    private SharedPreferences sharedPreferences;

    @Inject
    Storage(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void save(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public String load(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void saveState(String key, Boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public Boolean loadState(String key) {
        return sharedPreferences.getBoolean(key, false);
    }
}
