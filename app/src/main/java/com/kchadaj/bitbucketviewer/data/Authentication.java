package com.kchadaj.bitbucketviewer.data;

import android.net.Uri;

import dagger.Module;

@Module
public class Authentication {

    public final static String redirectUrl = "bitbucketviewer://callback";
    public final static String clientId = "********";
    public final static String clientSecret = "********";

    public final static Uri authCodeUri = Uri.parse("https://bitbucket.org/site/oauth2/authorize"
            + "?client_id=" + clientId
            + "&redirect_url=" + redirectUrl
            + "&scope=repository"
            + "&response_type=code");
}
