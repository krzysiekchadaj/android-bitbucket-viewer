package com.kchadaj.bitbucketviewer.model.commits;

public class Commit {

    private String hash;
    private String date;
    private String message;

    Commit() {
        // empty
    }

    public Commit(String hash, String date, String message) {
        this.hash = hash;
        this.date = date;
        this.message = message;
    }

    public String getHash() {
        return hash;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }
}
