package com.kchadaj.bitbucketviewer.model.commits;

import java.util.List;

public class Commits {

    private List<Commit> values;
    private String next;

    public Commits() {
        // empty
    }

    public List<Commit> getValues() {
        return values;
    }

    public String getNext() {
        return next;
    }
}
