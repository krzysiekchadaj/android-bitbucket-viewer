package com.kchadaj.bitbucketviewer.model.files;

public class Commit {

    private String hash;

    public Commit(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }
}
