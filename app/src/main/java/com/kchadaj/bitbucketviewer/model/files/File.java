package com.kchadaj.bitbucketviewer.model.files;

public class File {

    public static final String TYPE_DIRECTORY = "commit_directory";
    public static final String TYPE_FILE = "commit_file";

    private String path;
    private String type;
    private Commit commit;

    public File(String path, String type, Commit commit) {
        this.path = path;
        this.type = type;
        this.commit = commit;
    }

    public String getPath() {
        return path;
    }

    public String getType() {
        return type;
    }

    public Commit getCommit() {
        return commit;
    }
}
