package com.kchadaj.bitbucketviewer.model.repos;

public class RepoAvatar {

    private String href;

    public RepoAvatar() {
        // empty
    }

    RepoAvatar(String href) {
        this.href = href;
    }

    public String getHref() {
        return href;
    }
}
