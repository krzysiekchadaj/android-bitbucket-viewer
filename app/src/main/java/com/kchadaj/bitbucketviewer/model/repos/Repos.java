package com.kchadaj.bitbucketviewer.model.repos;

import java.util.List;

public class Repos {

    private List<Repo> values;
    private String next;

    public Repos() {
        // empty
    }

    public List<Repo> getValues() {
        return values;
    }

    public String getNext() {
        return next;
    }
}
