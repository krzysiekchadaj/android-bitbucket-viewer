package com.kchadaj.bitbucketviewer.model.repos;

public class Repo {

    private String name;
    private String description;
    private RepoLinks links;
    private String slug;

    public Repo() {
        // empty
    }

    public Repo(String name, String description, String avatarUrl) {
        this.name = name;
        this.description = description;
        this.links = new RepoLinks(avatarUrl);

        this.slug = "";
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public RepoLinks getLinks() {
        return links;
    }

    public String getSlug() {
        return slug;
    }
}
