package com.kchadaj.bitbucketviewer.model.files;

import java.util.List;

public class Files {

    private List<File> values;
    private String next;

    public Files() {
        // empty
    }

    public List<File> getValues() {
        return values;
    }

    public String getNext() {
        return next;
    }
}
