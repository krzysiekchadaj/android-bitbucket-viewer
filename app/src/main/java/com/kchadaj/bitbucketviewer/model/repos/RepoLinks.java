package com.kchadaj.bitbucketviewer.model.repos;

public class RepoLinks {

    private RepoAvatar avatar;

    public RepoLinks() {
        // empty
    }

    RepoLinks(String url) {
        this.avatar = new RepoAvatar(url);
    }

    public RepoAvatar getAvatar() {
        return avatar;
    }
}
