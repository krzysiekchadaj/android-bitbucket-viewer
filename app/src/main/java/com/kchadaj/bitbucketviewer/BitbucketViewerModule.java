package com.kchadaj.bitbucketviewer;

import com.kchadaj.bitbucketviewer.modules.ApiModule;
import com.kchadaj.bitbucketviewer.modules.ServiceModule;
import com.kchadaj.bitbucketviewer.modules.StorageModule;
import com.kchadaj.bitbucketviewer.screens.auth.AuthComponent;
import com.kchadaj.bitbucketviewer.screens.navigate.commits.CommitsComponent;
import com.kchadaj.bitbucketviewer.screens.repos.ReposComponent;

import dagger.Module;

@Module(includes = {ApiModule.class, ServiceModule.class, StorageModule.class},
        subcomponents = {AuthComponent.class, ReposComponent.class, CommitsComponent.class})
class BitbucketViewerModule {
    // empty
}
