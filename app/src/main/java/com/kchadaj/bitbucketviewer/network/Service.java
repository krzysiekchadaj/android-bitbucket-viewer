package com.kchadaj.bitbucketviewer.network;

import com.kchadaj.bitbucketviewer.data.Authentication;

import javax.inject.Inject;

import static com.kchadaj.bitbucketviewer.utils.NetworkUtils.call;

public class Service {
    private ServiceEndpoint serviceEndpoint;

    @Inject
    Service(ServiceEndpoint serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    public void getAccessToken(String code, final OnNetworkResultCallback callback) {
        call(serviceEndpoint.getAccessCode(
                Authentication.clientId,
                Authentication.clientSecret,
                code,
                "authorization_code"
        ), callback);
    }
}
