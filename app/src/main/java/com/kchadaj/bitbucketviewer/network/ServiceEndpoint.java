package com.kchadaj.bitbucketviewer.network;

import com.kchadaj.bitbucketviewer.model.auth.AccessToken;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ServiceEndpoint {
    String BASE_URL = "https://bitbucket.org/";

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("site/oauth2/access_token")
    Observable<AccessToken> getAccessCode(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("code") String code,
            @Field("grant_type") String grantType
    );
}
