package com.kchadaj.bitbucketviewer.network;

import javax.inject.Inject;

import static com.kchadaj.bitbucketviewer.utils.NetworkUtils.call;

public class Api {
    private ApiEndpoints apiEndpoints;

    @Inject
    Api(ApiEndpoints apiEndpoints) {
        this.apiEndpoints = apiEndpoints;
    }

    public void getRepos(String username,
                         String accessToken,
                         int page,
                         final OnNetworkResultCallback callback) {
        call(apiEndpoints.getRepos(username, accessToken, page), callback);
    }

    public void getCommits(String username,
                           String repoSlug,
                           String accessToken,
                           int page,
                           final OnNetworkResultCallback callback) {
        call(apiEndpoints.getCommits(username, repoSlug, accessToken, page), callback);
    }

    public void getFiles(String username,
                         String repoSlug,
                         String commitHash,
                         String path,
                         String accessToken,
                         int page,
                         final OnNetworkResultCallback callback) {

        if (commitHash == null || commitHash.isEmpty() || path == null || path.isEmpty()) {
            call(apiEndpoints.getFiles(username, repoSlug, accessToken, page), callback);
        } else {
            call(apiEndpoints.getFiles(username, repoSlug, commitHash, path, accessToken, page), callback);
        }
    }
}
