package com.kchadaj.bitbucketviewer.network;

import io.reactivex.disposables.Disposable;

public interface OnNetworkResultCallback {
    void onSuccess(Object result);

    void onError(Throwable t);

    void onStart(Disposable d);
}
