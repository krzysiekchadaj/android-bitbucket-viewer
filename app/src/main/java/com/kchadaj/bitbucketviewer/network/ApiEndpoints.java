package com.kchadaj.bitbucketviewer.network;

import com.kchadaj.bitbucketviewer.model.commits.Commits;
import com.kchadaj.bitbucketviewer.model.files.Files;
import com.kchadaj.bitbucketviewer.model.repos.Repos;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiEndpoints {
    String BASE_URL = "https://api.bitbucket.org/";

    @GET("/2.0/repositories/{username}")
    Observable<Repos> getRepos(
            @Path("username") String username,
            @Query("access_token") String accessToken,
            @Query("page") int page
    );

    @GET("/2.0/repositories/{username}/{repo_slug}/commits")
    Observable<Commits> getCommits(
            @Path("username") String username,
            @Path("repo_slug") String repoSlug,
            @Query("access_token") String accessToken,
            @Query("page") int page
    );

    @GET("/2.0/repositories/{username}/{repo_slug}/src")
    Observable<Files> getFiles(
            @Path("username") String username,
            @Path("repo_slug") String repoSlug,
            @Query("access_token") String accessToken,
            @Query("page") int page
    );

    @GET("/2.0/repositories/{username}/{repo_slug}/src/{commit_hash}/{path}")
    Observable<Files> getFiles(
            @Path("username") String username,
            @Path("repo_slug") String repoSlug,
            @Path("commit_hash") String commitHash,
            @Path("path") String path,
            @Query("access_token") String accessToken,
            @Query("page") int page
    );
}
