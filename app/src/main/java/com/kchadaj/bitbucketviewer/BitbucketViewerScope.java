package com.kchadaj.bitbucketviewer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface BitbucketViewerScope {
    // empty
}
