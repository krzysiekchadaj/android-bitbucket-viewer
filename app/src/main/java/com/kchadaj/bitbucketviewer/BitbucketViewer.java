package com.kchadaj.bitbucketviewer;

import android.app.Application;

import com.kchadaj.bitbucketviewer.modules.ContextModule;
import com.squareup.leakcanary.LeakCanary;

import timber.log.Timber;

public class BitbucketViewer extends Application {

    private static BitbucketViewerComponent component;

    public static BitbucketViewerComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) return;
        LeakCanary.install(this);

        Timber.plant(new Timber.DebugTree());

        component = DaggerBitbucketViewerComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
    }
}
