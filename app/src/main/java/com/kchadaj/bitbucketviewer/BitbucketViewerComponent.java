package com.kchadaj.bitbucketviewer;

import android.content.Context;

import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.screens.auth.AuthComponent;
import com.kchadaj.bitbucketviewer.screens.auth.AuthModule;
import com.kchadaj.bitbucketviewer.screens.navigate.commits.CommitsComponent;
import com.kchadaj.bitbucketviewer.screens.navigate.commits.CommitsModule;
import com.kchadaj.bitbucketviewer.screens.navigate.files.FilesComponent;
import com.kchadaj.bitbucketviewer.screens.navigate.files.FilesModule;
import com.kchadaj.bitbucketviewer.screens.repos.ReposComponent;
import com.kchadaj.bitbucketviewer.screens.repos.ReposModule;
import com.kchadaj.bitbucketviewer.screens.viewer.ViewerComponent;
import com.kchadaj.bitbucketviewer.screens.viewer.ViewerModule;

import dagger.Component;

@BitbucketViewerScope
@Component(modules = BitbucketViewerModule.class)
public interface BitbucketViewerComponent {

    AuthComponent plus(AuthModule authModule);

    ReposComponent plus(ReposModule reposModule);

    FilesComponent plus(FilesModule filesModule);

    CommitsComponent plus(CommitsModule commitsModule);

    ViewerComponent plus(ViewerModule viewerModule);

    Context getContext();

    Storage getStorage();
}
