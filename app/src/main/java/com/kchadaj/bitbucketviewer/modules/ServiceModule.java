package com.kchadaj.bitbucketviewer.modules;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;
import com.kchadaj.bitbucketviewer.network.ServiceEndpoint;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {OkHttpClientModule.class})
public class ServiceModule {

    @Provides
    @BitbucketViewerScope
    ServiceEndpoint provideServiceEndpoints(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(ServiceEndpoint.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ServiceEndpoint.class);
    }
}
