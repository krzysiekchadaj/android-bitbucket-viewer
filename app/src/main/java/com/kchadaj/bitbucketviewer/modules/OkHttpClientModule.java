package com.kchadaj.bitbucketviewer.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

@Module(includes = ContextModule.class)
public class OkHttpClientModule {
    private static final long CACHE_SIZE = 1024 * 1024; // 1MB
    private static final String CACHE_SUB_DIR = "cache";

    @Provides
    @BitbucketViewerScope
    Cache providesCache(Context context) {
        File cacheDir = context.getCacheDir();
        File cacheFile = new File(cacheDir, CACHE_SUB_DIR);
        //noinspection ResultOfMethodCallIgnored
        cacheFile.mkdirs();
        return new Cache(cacheFile, CACHE_SIZE);
    }

    @Provides
    @BitbucketViewerScope
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(
                new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(@NonNull String message) {
                        Timber.i(message);
                    }
                });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return interceptor;
    }

    @Provides
    @BitbucketViewerScope
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Cache cache) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }
}
