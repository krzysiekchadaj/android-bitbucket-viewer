package com.kchadaj.bitbucketviewer.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;
import com.kchadaj.bitbucketviewer.data.Storage;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class StorageModule {

    private static String sharedPreferencesFileKey = Storage.class.getName() + ".file";

    @Provides
    @BitbucketViewerScope
    public SharedPreferences providesSharedPreferences(Context context) {
        return context.getSharedPreferences(sharedPreferencesFileKey, Context.MODE_PRIVATE);
    }
}
