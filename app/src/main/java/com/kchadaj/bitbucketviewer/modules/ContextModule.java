package com.kchadaj.bitbucketviewer.modules;

import android.content.Context;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @BitbucketViewerScope
    Context providesContext() {
        return context;
    }
}
