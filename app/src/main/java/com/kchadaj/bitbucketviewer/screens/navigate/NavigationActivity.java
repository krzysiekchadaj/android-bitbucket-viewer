package com.kchadaj.bitbucketviewer.screens.navigate;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.screens.navigate.commits.CommitsFragment;
import com.kchadaj.bitbucketviewer.screens.navigate.files.FilesFragment;

public class NavigationActivity extends AppCompatActivity implements FilesFragment.Notifications {

    private ActiveMenu activeMenu;
    private int fragmentsCount;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_files:
                    if (!ActiveMenu.FILES.equals(activeMenu)) {
                        startFilesFragment();
                        activeMenu = ActiveMenu.FILES;
                    }
                    return true;

                case R.id.navigation_commits:
                    if (!ActiveMenu.COMMITS.equals(activeMenu)) {
                        startCommitsFragment();
                        activeMenu = ActiveMenu.COMMITS;
                    }
                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        startFilesFragment();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {

        fragmentsCount--;
        if (fragmentsCount < 0) {
            finish();
        }

        super.onBackPressed();
    }

    @Override
    public void onNewFilesFragment() {
        fragmentsCount++;
    }

    private void startFilesFragment() {
        resetFilesScreenStorage();
        startFragment(FilesFragment.newInstance());
    }

    private void startCommitsFragment() {
        startFragment(CommitsFragment.newInstance());
    }

    private void resetFilesScreenStorage() {
        BitbucketViewer.getComponent().getStorage().save(Storage.FILES_COMMIT_HASH, "");
        BitbucketViewer.getComponent().getStorage().save(Storage.FILES_PATH, "");
    }

    private void startFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.navigation_container, fragment)
                .commit();

        fragmentsCount = 0;
    }

    private enum ActiveMenu {
        FILES,
        COMMITS
    }
}
