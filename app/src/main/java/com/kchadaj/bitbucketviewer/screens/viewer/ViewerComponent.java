package com.kchadaj.bitbucketviewer.screens.viewer;

import dagger.Subcomponent;

@ViewerScope
@Subcomponent(modules = ViewerModule.class)
public interface ViewerComponent {

    void inject(ViewerFragment viewerFragment);
}
