package com.kchadaj.bitbucketviewer.screens.navigate.files;

import com.kchadaj.bitbucketviewer.utils.Errors;

interface FilesViewContract {

    void startNewFilesScreen();

    void startFileViewerScreen();

    void showProgressBar();

    void hideProgressBar();

    void notifyUser(Errors error);
}
