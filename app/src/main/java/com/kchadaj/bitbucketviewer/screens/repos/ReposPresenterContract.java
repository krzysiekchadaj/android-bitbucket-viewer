package com.kchadaj.bitbucketviewer.screens.repos;

public interface ReposPresenterContract {

    ReposAdapter getReposAdapter();

    void requestUserRepos();

    void lastItemReached();

    void onDestroyView();
}
