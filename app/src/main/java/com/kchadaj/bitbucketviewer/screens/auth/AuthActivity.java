package com.kchadaj.bitbucketviewer.screens.auth;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.kchadaj.bitbucketviewer.R;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        FragmentManager manager = getSupportFragmentManager();
        AuthFragment authFragment = (AuthFragment) manager.findFragmentById(R.id.auth_container);
        if (authFragment == null) {
            authFragment = AuthFragment.newInstance();
            manager.beginTransaction()
                    .add(R.id.auth_container, authFragment)
                    .commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
