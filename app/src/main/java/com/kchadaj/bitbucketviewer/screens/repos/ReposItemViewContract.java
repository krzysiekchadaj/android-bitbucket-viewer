package com.kchadaj.bitbucketviewer.screens.repos;

public interface ReposItemViewContract {

    void setRepoTitle(String title);

    void setRepoDescription(String description);

    void setRepoAvatar(String avatarUrl);
}
