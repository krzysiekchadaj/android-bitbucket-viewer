package com.kchadaj.bitbucketviewer.screens.auth;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthModule {

    private AuthViewContract authViewContract;

    AuthModule(AuthViewContract authViewContract) {
        this.authViewContract = authViewContract;
    }

    @AuthScope
    @Provides
    AuthViewContract providesAuthViewContract() {
        return authViewContract;
    }
}
