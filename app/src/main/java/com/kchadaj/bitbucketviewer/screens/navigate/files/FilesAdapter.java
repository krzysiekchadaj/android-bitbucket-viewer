package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kchadaj.bitbucketviewer.R;

import javax.inject.Inject;

public class FilesAdapter extends RecyclerView.Adapter<FilesViewHolder> {

    private final FilesItemPresenter filesItemPresenter;

    @Inject
    FilesAdapter(FilesItemPresenter filesItemPresenter) {
        this.filesItemPresenter = filesItemPresenter;
    }

    @NonNull
    @Override
    public FilesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View repoCard = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.card_view_files, parent, false);
        return new FilesViewHolder(repoCard, filesItemPresenter);
    }

    @Override
    public void onBindViewHolder(@NonNull FilesViewHolder holder, int position) {
        filesItemPresenter.onBindFileAtPosition(holder, position);
    }

    @Override
    public int getItemCount() {
        return filesItemPresenter.getFilesCount();
    }
}
