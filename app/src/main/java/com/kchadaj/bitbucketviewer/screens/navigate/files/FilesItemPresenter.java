package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.support.annotation.NonNull;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.model.files.File;
import com.kchadaj.bitbucketviewer.utils.Errors;

import java.util.List;

import javax.inject.Inject;

public class FilesItemPresenter implements FilesItemPresenterContract {

    private final List<File> files;
    private final FilesViewContract filesViewContract;

    @Inject
    FilesItemPresenter(List<File> files, FilesViewContract filesViewContract) {
        this.files = files;
        this.filesViewContract = filesViewContract;
    }

    @Override
    public int getFilesCount() {
        return files.size();
    }

    @Override
    public void onBindFileAtPosition(@NonNull FilesItemViewContract itemView, int position) {
        final File file = files.get(position);
        itemView.setFilePath(file.getPath());

    }

    @Override
    public void onFileClickAtPosition(int position) {
        File file = files.get(position);
        switch (file.getType()) {
            case File.TYPE_DIRECTORY:
                BitbucketViewer.getComponent().getStorage().save(Storage.FILES_COMMIT_HASH, file.getCommit().getHash());
                BitbucketViewer.getComponent().getStorage().save(Storage.FILES_PATH, file.getPath());
                filesViewContract.startNewFilesScreen();
                break;

            case File.TYPE_FILE:
                BitbucketViewer.getComponent().getStorage().save(Storage.FILES_COMMIT_HASH, file.getCommit().getHash());
                BitbucketViewer.getComponent().getStorage().save(Storage.FILES_PATH, file.getPath());
                filesViewContract.startFileViewerScreen();
                break;

            default:
                filesViewContract.notifyUser(Errors.FILE_TYPE_UNKNOWN);
                break;
        }
    }
}
