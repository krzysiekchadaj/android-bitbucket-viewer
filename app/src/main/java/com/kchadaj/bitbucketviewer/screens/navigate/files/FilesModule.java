package com.kchadaj.bitbucketviewer.screens.navigate.files;

import com.kchadaj.bitbucketviewer.model.files.File;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class FilesModule {

    private FilesViewContract filesViewContract;
    private List<File> files;

    FilesModule(FilesViewContract filesViewContract) {
        this.filesViewContract = filesViewContract;
        initFiles();
    }

    @FilesScope
    @Provides
    FilesViewContract providesFilesViewContract() {
        return filesViewContract;
    }

    @FilesScope
    @Provides
    List<File> providesFiles() {
        return files;
    }

    private void initFiles() {
        this.files = new ArrayList<>(0);
    }
}
