package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.kchadaj.bitbucketviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class FilesViewHolder extends RecyclerView.ViewHolder implements FilesItemViewContract {

    @BindView(R.id.file_path_text_view)
    TextView filePath;

    FilesViewHolder(View itemView, final FilesItemPresenterContract filesItemPresenter) {
        super(itemView);

        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filesItemPresenter.onFileClickAtPosition(getAdapterPosition());
            }
        });
    }

    @Override
    public void setFilePath(String path) {
        if (!TextUtils.isEmpty(path)) {
            filePath.setText(path);
        } else {
            filePath.setText(" ");
        }
    }
}
