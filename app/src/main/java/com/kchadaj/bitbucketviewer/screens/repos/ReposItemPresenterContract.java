package com.kchadaj.bitbucketviewer.screens.repos;

import android.support.annotation.NonNull;

public interface ReposItemPresenterContract {

    int getReposCount();

    void onBindRepoAtPosition(@NonNull ReposItemViewContract itemView, int position);

    void onRepoClickAtPosition(int position);
}
