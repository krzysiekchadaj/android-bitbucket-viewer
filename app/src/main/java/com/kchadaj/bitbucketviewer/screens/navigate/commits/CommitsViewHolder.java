package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.kchadaj.bitbucketviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class CommitsViewHolder extends RecyclerView.ViewHolder implements CommitsItemViewContract {

    @BindView(R.id.commits_hash_text_view)
    TextView commitHash;

    @BindView(R.id.commits_date_text_view)
    TextView commitDate;

    @BindView(R.id.commits_message_text_view)
    TextView commitMessage;

    CommitsViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setCommitHash(String hash) {
        setText(commitHash, hash);
    }

    @Override
    public void setCommitDate(String date) {
        setText(commitDate, date);
    }

    @Override
    public void setCommitMessage(String message) {
        setText(commitMessage, message);
    }

    private void setText(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
        } else {
            textView.setText(" ");
        }
    }
}
