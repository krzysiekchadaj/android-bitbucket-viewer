package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import javax.inject.Scope;

@Scope
@interface CommitsScope {
    // empty
}
