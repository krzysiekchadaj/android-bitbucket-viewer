package com.kchadaj.bitbucketviewer.screens.repos;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kchadaj.bitbucketviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class ReposViewHolder extends RecyclerView.ViewHolder implements ReposItemViewContract {

    private final View view;

    @BindView(R.id.repo_title_text_view)
    TextView repoTitle;

    @BindView(R.id.repo_description_text_view)
    TextView repoDescription;

    @BindView(R.id.repo_avatar)
    ImageView repoAvatar;

    ReposViewHolder(View itemView, final ReposItemPresenterContract reposItemPresenter) {
        super(itemView);

        ButterKnife.bind(this, itemView);

        view = itemView;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reposItemPresenter.onRepoClickAtPosition(getAdapterPosition());
            }
        });
    }

    @Override
    public void setRepoTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            repoTitle.setText(title);
        } else {
            repoTitle.setText(" ");
        }
    }

    @Override
    public void setRepoDescription(String description) {
        if (!TextUtils.isEmpty(description)) {
            repoDescription.setText(description);
        } else {
            repoDescription.setText(" ");
        }
    }

    @Override
    public void setRepoAvatar(String avatarUrl) {
        Glide.with(view)
                .asBitmap()
                .load(avatarUrl)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.default_repo_icon)
                        .fitCenter())
                .into(repoAvatar);
    }
}
