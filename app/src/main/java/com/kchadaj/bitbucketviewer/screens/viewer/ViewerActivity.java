package com.kchadaj.bitbucketviewer.screens.viewer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.kchadaj.bitbucketviewer.R;

public class ViewerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        FragmentManager manager = getSupportFragmentManager();
        ViewerFragment viewerFragment = (ViewerFragment) manager.findFragmentById(R.id.viewer_container);
        if (viewerFragment == null) {
            viewerFragment = ViewerFragment.newInstance();
            manager.beginTransaction()
                    .add(R.id.viewer_container, viewerFragment)
                    .commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
