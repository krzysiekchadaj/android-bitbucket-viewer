package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import com.kchadaj.bitbucketviewer.utils.Errors;

interface CommitsViewContract {

    void showProgressBar();

    void hideProgressBar();

    void notifyUser(Errors error);
}
