package com.kchadaj.bitbucketviewer.screens.auth;

import android.net.Uri;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Authentication;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.model.auth.AccessToken;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;
import com.kchadaj.bitbucketviewer.network.Service;
import com.kchadaj.bitbucketviewer.utils.Errors;
import com.kchadaj.bitbucketviewer.utils.NetworkUtils;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class AuthPresenter implements AuthPresenterContract {

    private final AuthViewContract view;
    private final Service service;

    private Disposable networkRequestsDisposable;

    @Inject
    AuthPresenter(AuthViewContract view, Service service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void onAuthenticateButtonClick(String username) {

        if (!NetworkUtils.isNetworkAvailable()) {
            view.notifyUser(Errors.NO_INTERNET);
            return;
        }

        if (!isUsernameValid(username)) {
            view.notifyUser(Errors.USERNAME_INVALID);
            return;
        }

        BitbucketViewer.getComponent().getStorage().save(Storage.USERNAME, username);
        BitbucketViewer.getComponent().getStorage().saveState(Storage.IS_AUTH_URI_EXPECTED, true);

        view.hideSoftKeyboard();
        view.requestAuthCode(Authentication.authCodeUri);
    }

    @Override
    public void onIncomingAuthUri(Uri uri) {
        if (uri != null &&
                uri.toString().startsWith(Authentication.redirectUrl) &&
                BitbucketViewer.getComponent().getStorage().loadState(Storage.IS_AUTH_URI_EXPECTED)) {
            String authCode = uri.getQueryParameter("code");
            requestAccessToken(authCode);
        }
    }

    @Override
    public String getLastUsername() {
        return BitbucketViewer.getComponent().getStorage().load(Storage.USERNAME);
    }

    @Override
    public void onDestroyView() {
        if ((networkRequestsDisposable != null) && (!networkRequestsDisposable.isDisposed())) {
            networkRequestsDisposable.dispose();
        }
    }

    private void requestAccessToken(String authCode) {
        BitbucketViewer.getComponent().getStorage().saveState(Storage.IS_AUTH_URI_EXPECTED, false);
        service.getAccessToken(authCode, new OnNetworkResultCallback() {
            @Override
            public void onSuccess(Object result) {
                String accessToken = ((AccessToken) result).getAccessToken();
                BitbucketViewer.getComponent().getStorage().save(Storage.ACCESS_TOKEN, accessToken);
                view.startReposActivity();
            }

            @Override
            public void onError(Throwable t) {
                Timber.e(t, "Authentication failed: %s", t.getLocalizedMessage());
                view.notifyUser(Errors.AUTHENTICATION_FAILED);
            }

            @Override
            public void onStart(Disposable d) {
                networkRequestsDisposable = d;
            }
        });
    }

    private Boolean isUsernameValid(String username) {
        return (username != null) && (!username.isEmpty());
    }
}
