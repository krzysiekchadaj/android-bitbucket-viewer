package com.kchadaj.bitbucketviewer.screens.repos;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.model.repos.Repo;
import com.kchadaj.bitbucketviewer.model.repos.Repos;
import com.kchadaj.bitbucketviewer.network.Api;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;
import com.kchadaj.bitbucketviewer.utils.Errors;
import com.kchadaj.bitbucketviewer.utils.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class ReposPresenter implements ReposPresenterContract {

    private final ReposViewContract view;
    private final Api api;
    private final List<Repo> repositories;
    private final ReposAdapter reposAdapter;

    private Disposable networkRequestsDisposable;

    private int currentPage;
    private boolean isMoreDataToLoad;
    private boolean isViewRefreshing;

    @Inject
    ReposPresenter(ReposViewContract view, Api api, List<Repo> repositories, ReposAdapter reposAdapter) {
        this.view = view;
        this.api = api;
        this.repositories = repositories;
        this.reposAdapter = reposAdapter;
    }

    public List<Repo> getRepositories() {
        return repositories;
    }

    @Override
    public ReposAdapter getReposAdapter() {
        return reposAdapter;
    }

    @Override
    public void lastItemReached() {
        if (isMoreDataToLoad && !isViewRefreshing) {
            if (NetworkUtils.isNetworkAvailable()) {
                getNextRepos();
            } else {
                view.notifyUser(Errors.NO_INTERNET_LOAD_MORE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        if ((networkRequestsDisposable != null) && (!networkRequestsDisposable.isDisposed())) {
            networkRequestsDisposable.dispose();
        }
    }

    @Override
    public void requestUserRepos() {
        if (NetworkUtils.isNetworkAvailable()) {
            currentPage = 1;
            repositories.clear();
            getRepos();
        } else {
            view.notifyUser(Errors.NO_INTERNET);
        }
    }

    private void getNextRepos() {
        currentPage++;
        getRepos();
    }

    private void getRepos() {
        isViewRefreshing = true;
        view.showProgressBar();
        api.getRepos(
                BitbucketViewer.getComponent().getStorage().load(Storage.USERNAME),
                BitbucketViewer.getComponent().getStorage().load(Storage.ACCESS_TOKEN),
                currentPage,
                new OnNetworkResultCallback() {
                    @Override
                    public void onSuccess(Object result) {
                        Repos userRepos = (Repos) result;

                        repositories.addAll(userRepos.getValues());
                        reposAdapter.notifyDataSetChanged();

                        String next = userRepos.getNext();
                        isMoreDataToLoad = (next != null) && !(next.isEmpty());

                        isViewRefreshing = false;
                        view.hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t, "Failed to load: %s", t.getLocalizedMessage());
                        isViewRefreshing = false;
                        view.hideProgressBar();
                        view.notifyUser(Errors.LOADING_FAILED);
                    }

                    @Override
                    public void onStart(Disposable d) {
                        networkRequestsDisposable = d;
                    }
                });
    }
}
