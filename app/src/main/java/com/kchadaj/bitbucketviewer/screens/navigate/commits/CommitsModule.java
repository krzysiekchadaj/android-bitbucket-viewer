package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import com.kchadaj.bitbucketviewer.model.commits.Commit;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class CommitsModule {

    private CommitsViewContract commitsViewContract;
    private List<Commit> commits;

    CommitsModule(CommitsViewContract commitsViewContract) {
        this.commitsViewContract = commitsViewContract;
        initCommits();
    }

    @CommitsScope
    @Provides
    CommitsViewContract providesCommitsViewContract() {
        return commitsViewContract;
    }

    @CommitsScope
    @Provides
    List<Commit> providesCommits() {
        return commits;
    }

    private void initCommits() {
        this.commits = new ArrayList<>(0);
    }
}
