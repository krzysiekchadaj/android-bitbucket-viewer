package com.kchadaj.bitbucketviewer.screens.viewer;

import javax.inject.Scope;

@Scope
@interface ViewerScope {
    // empty
}
