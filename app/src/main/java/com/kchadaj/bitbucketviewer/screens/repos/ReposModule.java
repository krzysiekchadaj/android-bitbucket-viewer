package com.kchadaj.bitbucketviewer.screens.repos;

import com.kchadaj.bitbucketviewer.model.repos.Repo;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class ReposModule {

    private ReposViewContract reposViewContract;
    private List<Repo> repositories;

    ReposModule(ReposViewContract reposViewContract) {
        this.reposViewContract = reposViewContract;
        initRepositories();
    }

    @ReposScope
    @Provides
    ReposViewContract providesReposViewContract() {
        return reposViewContract;
    }

    @ReposScope
    @Provides
    List<Repo> providesRepositories() {
        return repositories;
    }

    private void initRepositories() {
        this.repositories = new ArrayList<>(0);
    }
}
