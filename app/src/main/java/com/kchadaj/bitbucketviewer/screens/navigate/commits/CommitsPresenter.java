package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.model.commits.Commit;
import com.kchadaj.bitbucketviewer.model.commits.Commits;
import com.kchadaj.bitbucketviewer.network.Api;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;
import com.kchadaj.bitbucketviewer.utils.Errors;
import com.kchadaj.bitbucketviewer.utils.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

class CommitsPresenter implements CommitsPresenterContract {

    private final CommitsViewContract view;
    private final Api api;
    private final List<Commit> commits;
    private final CommitsAdapter commitsAdapter;

    private Disposable networkRequestsDisposable;

    private int currentPage;
    private boolean isMoreDataToLoad;
    private boolean isViewRefreshing;

    @Inject
    CommitsPresenter(CommitsViewContract view, Api api, List<Commit> commits, CommitsAdapter commitsAdapter) {
        this.view = view;
        this.api = api;
        this.commits = commits;
        this.commitsAdapter = commitsAdapter;
    }

    public List<Commit> getCommits() {
        return commits;
    }

    @Override
    public CommitsAdapter getCommitsAdapter() {
        return commitsAdapter;
    }

    @Override
    public void lastItemReached() {
        if (isMoreDataToLoad && !isViewRefreshing) {
            if (NetworkUtils.isNetworkAvailable()) {
                requestNextCommits();
            } else {
                view.notifyUser(Errors.NO_INTERNET_LOAD_MORE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        if ((networkRequestsDisposable != null) && (!networkRequestsDisposable.isDisposed())) {
            networkRequestsDisposable.dispose();
        }
    }

    @Override
    public void requestCommits() {
        if (NetworkUtils.isNetworkAvailable()) {
            currentPage = 1;
            commits.clear();
            downloadCommits();
        } else {
            view.notifyUser(Errors.NO_INTERNET);
        }
    }

    private void requestNextCommits() {
        currentPage++;
        downloadCommits();
    }

    private void downloadCommits() {
        isViewRefreshing = true;
        view.showProgressBar();
        api.getCommits(
                BitbucketViewer.getComponent().getStorage().load(Storage.USERNAME),
                BitbucketViewer.getComponent().getStorage().load(Storage.REPO_SLUG),
                BitbucketViewer.getComponent().getStorage().load(Storage.ACCESS_TOKEN),
                currentPage,
                new OnNetworkResultCallback() {
                    @Override
                    public void onSuccess(Object result) {
                        Commits userCommits = (Commits) result;

                        commits.addAll(userCommits.getValues());
                        commitsAdapter.notifyDataSetChanged();

                        String next = userCommits.getNext();
                        isMoreDataToLoad = (next != null) && !(next.isEmpty());

                        isViewRefreshing = false;
                        view.hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t, "Failed to load: %s", t.getLocalizedMessage());
                        isViewRefreshing = false;
                        view.hideProgressBar();
                        view.notifyUser(Errors.LOADING_FAILED);
                    }

                    @Override
                    public void onStart(Disposable d) {
                        networkRequestsDisposable = d;
                    }
                });
    }
}
