package com.kchadaj.bitbucketviewer.screens.navigate.files;

public interface FilesPresenterContract {

    FilesAdapter getFilesAdapter();

    void requestFiles();

    void lastItemReached();

    void onDestroyView();
}
