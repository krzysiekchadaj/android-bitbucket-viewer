package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.support.annotation.NonNull;

public interface CommitsItemPresenterContract {

    int getCommitsCount();

    void onBindCommitAtPosition(@NonNull CommitsViewHolder holder, int position);
}
