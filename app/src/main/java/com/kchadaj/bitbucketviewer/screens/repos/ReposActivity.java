package com.kchadaj.bitbucketviewer.screens.repos;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.kchadaj.bitbucketviewer.R;

public class ReposActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos);

        FragmentManager manager = getSupportFragmentManager();
        ReposFragment reposFragment = (ReposFragment) manager.findFragmentById(R.id.repos_container);
        if (reposFragment == null) {
            reposFragment = ReposFragment.newInstance();
            manager.beginTransaction()
                    .add(R.id.repos_container, reposFragment)
                    .commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
