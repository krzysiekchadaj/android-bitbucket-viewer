package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.support.annotation.NonNull;

public interface FilesItemPresenterContract {

    int getFilesCount();

    void onBindFileAtPosition(@NonNull FilesItemViewContract itemView, int position);

    void onFileClickAtPosition(int position);
}
