package com.kchadaj.bitbucketviewer.screens.navigate.files;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.model.files.File;
import com.kchadaj.bitbucketviewer.model.files.Files;
import com.kchadaj.bitbucketviewer.network.Api;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;
import com.kchadaj.bitbucketviewer.utils.Errors;
import com.kchadaj.bitbucketviewer.utils.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class FilesPresenter implements FilesPresenterContract {

    private final FilesViewContract view;
    private final Api api;
    private final List<File> files;
    private final FilesAdapter filesAdapter;

    private Disposable networkRequestsDisposable;

    private int currentPage;
    private boolean isMoreDataToLoad;
    private boolean isViewRefreshing;

    @Inject
    FilesPresenter(FilesViewContract view, Api api, List<File> files, FilesAdapter filesAdapter) {
        this.view = view;
        this.api = api;
        this.files = files;
        this.filesAdapter = filesAdapter;
    }

    public List<File> getFiles() {
        return files;
    }

    @Override
    public FilesAdapter getFilesAdapter() {
        return filesAdapter;
    }

    @Override
    public void lastItemReached() {
        if (isMoreDataToLoad && !isViewRefreshing) {
            if (NetworkUtils.isNetworkAvailable()) {
                requestNextFiles();
            } else {
                view.notifyUser(Errors.NO_INTERNET_LOAD_MORE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        if ((networkRequestsDisposable != null) && (!networkRequestsDisposable.isDisposed())) {
            networkRequestsDisposable.dispose();
        }
    }

    @Override
    public void requestFiles() {
        if (NetworkUtils.isNetworkAvailable()) {
            currentPage = 1;
            files.clear();
            downloadFiles();
        } else {
            view.notifyUser(Errors.NO_INTERNET);
        }
    }

    private void requestNextFiles() {
        currentPage++;
        downloadFiles();
    }

    private void downloadFiles() {
        isViewRefreshing = true;
        view.showProgressBar();
        api.getFiles(
                BitbucketViewer.getComponent().getStorage().load(Storage.USERNAME),
                BitbucketViewer.getComponent().getStorage().load(Storage.REPO_SLUG),
                BitbucketViewer.getComponent().getStorage().load(Storage.FILES_COMMIT_HASH),
                BitbucketViewer.getComponent().getStorage().load(Storage.FILES_PATH),
                BitbucketViewer.getComponent().getStorage().load(Storage.ACCESS_TOKEN),
                currentPage,
                new OnNetworkResultCallback() {
                    @Override
                    public void onSuccess(Object result) {
                        Files userFiles = (Files) result;

                        files.addAll(userFiles.getValues());
                        filesAdapter.notifyDataSetChanged();

                        String next = userFiles.getNext();
                        isMoreDataToLoad = (next != null) && !(next.isEmpty());

                        isViewRefreshing = false;
                        view.hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t, "Failed to load: %s", t.getLocalizedMessage());
                        isViewRefreshing = false;
                        view.hideProgressBar();
                        view.notifyUser(Errors.LOADING_FAILED);
                    }

                    @Override
                    public void onStart(Disposable d) {
                        networkRequestsDisposable = d;
                    }
                });
    }
}
