package com.kchadaj.bitbucketviewer.screens.auth;

import dagger.Subcomponent;

@AuthScope
@Subcomponent(modules = AuthModule.class)
public interface AuthComponent {

    void inject(AuthFragment authFragment);
}
