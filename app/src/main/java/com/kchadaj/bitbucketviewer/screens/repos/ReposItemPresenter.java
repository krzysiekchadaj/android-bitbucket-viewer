package com.kchadaj.bitbucketviewer.screens.repos;

import android.support.annotation.NonNull;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.model.repos.Repo;

import java.util.List;

import javax.inject.Inject;

public class ReposItemPresenter implements ReposItemPresenterContract {

    private final List<Repo> repositories;
    private final ReposViewContract reposViewContract;

    @Inject
    ReposItemPresenter(List<Repo> repositories, ReposViewContract reposViewContract) {
        this.repositories = repositories;
        this.reposViewContract = reposViewContract;
    }

    @Override
    public int getReposCount() {
        return repositories.size();
    }

    @Override
    public void onBindRepoAtPosition(@NonNull ReposItemViewContract itemView, int position) {
        final Repo repository = repositories.get(position);
        itemView.setRepoTitle(repository.getName());
        itemView.setRepoDescription(repository.getDescription());
        itemView.setRepoAvatar(repository.getLinks().getAvatar().getHref());
    }

    @Override
    public void onRepoClickAtPosition(int position) {
        BitbucketViewer.getComponent().getStorage().save(Storage.REPO_SLUG, repositories.get(position).getSlug());
        reposViewContract.startNavigationActivity();
    }
}
