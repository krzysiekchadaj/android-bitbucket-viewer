package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.screens.viewer.ViewerActivity;
import com.kchadaj.bitbucketviewer.utils.Errors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FilesFragment extends Fragment implements FilesViewContract {

    public static final String TAG = FilesFragment.class.getSimpleName();

    @BindView(R.id.recycler_files)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private Unbinder unbinder;

    private View view;

    private FilesPresenter filesPresenter;

    private Notifications callback;

    public FilesFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static FilesFragment newInstance() {
        return new FilesFragment();
    }

    public FilesPresenter getFilesPresenter() {
        return filesPresenter;
    }

    @Inject
    public void setFilesPresenter(FilesPresenter filesPresenter) {
        this.filesPresenter = filesPresenter;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        callback = (Notifications) getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BitbucketViewer.getComponent()
                .plus(new FilesModule(this))
                .inject(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_files, container, false);

        unbinder = ButterKnife.bind(this, view);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(filesPresenter.getFilesAdapter());
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    filesPresenter.lastItemReached();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        filesPresenter.requestFiles();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        recyclerView.stopScroll();
        recyclerView.addOnScrollListener(null);

        unbinder.unbind();

        filesPresenter.onDestroyView();
    }

    @Override
    public void startNewFilesScreen() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.getSupportFragmentManager().beginTransaction()
                    .hide(this)
                    .add(R.id.navigation_container, FilesFragment.newInstance())
                    .addToBackStack(TAG)
                    .commit();

            callback.onNewFilesFragment();
        }
    }

    @Override
    public void startFileViewerScreen() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.overridePendingTransition(0, 0);
            startActivity(new Intent(activity, ViewerActivity.class));
        }
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void notifyUser(Errors error) {
        Snackbar.make(view, getString(error.getId()), Snackbar.LENGTH_SHORT).show();
    }

    public interface Notifications {
        void onNewFilesFragment();
    }
}
