package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.support.annotation.NonNull;

import com.kchadaj.bitbucketviewer.model.commits.Commit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class CommitsItemPresenter implements CommitsItemPresenterContract {

    private final List<Commit> commits;

    @Inject
    CommitsItemPresenter(List<Commit> commits) {
        this.commits = commits;
    }

    @Override
    public int getCommitsCount() {
        return commits.size();
    }

    @Override
    public void onBindCommitAtPosition(@NonNull CommitsViewHolder itemView, int position) {
        final Commit commit = commits.get(position);

        String hash = commit.getHash().substring(0, 7);
        String message = commit.getMessage().trim();
        String date = formatDate(commit.getDate());

        itemView.setCommitHash(hash);
        itemView.setCommitMessage(message);
        itemView.setCommitDate(date);
    }

    private String formatDate(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.US);
            Date d = format.parse(date);
            format.applyPattern("HH:mm dd-MM-yyyy");
            return format.format(d);
        } catch (ParseException e) {
            return date;
        }
    }
}
