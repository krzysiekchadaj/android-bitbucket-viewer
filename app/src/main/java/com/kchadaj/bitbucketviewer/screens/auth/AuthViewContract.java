package com.kchadaj.bitbucketviewer.screens.auth;

import android.net.Uri;

import com.kchadaj.bitbucketviewer.utils.Errors;

public interface AuthViewContract {
    void requestAuthCode(Uri authCode);

    void hideSoftKeyboard();

    void startReposActivity();

    void notifyUser(Errors error);
}
