package com.kchadaj.bitbucketviewer.screens.viewer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.utils.Errors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ViewerFragment extends Fragment implements ViewerViewContract {

    @BindView(R.id.file_viewer_web_view)
    WebView fileViewerWebView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private Unbinder unbinder;

    private View view;

    private ViewerPresenter viewerPresenter;

    public ViewerFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static ViewerFragment newInstance() {
        return new ViewerFragment();
    }

    @Inject
    public void setViewerPresenter(ViewerPresenter viewerPresenter) {
        this.viewerPresenter = viewerPresenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BitbucketViewer.getComponent()
                .plus(new ViewerModule(this))
                .inject(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewer, container, false);

        unbinder = ButterKnife.bind(this, view);

        fileViewerSetup();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewerPresenter.onViewCreated();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    @Override
    public void loadFile(String fileURL) {
        fileViewerWebView.loadUrl(fileURL);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void notifyUser(Errors error) {
        Snackbar.make(view, getString(error.getId()), Snackbar.LENGTH_SHORT).show();
    }

    private void fileViewerSetup() {
        fileViewerWebView.getSettings().setUseWideViewPort(true);
        fileViewerWebView.getSettings().setBuiltInZoomControls(true);
        fileViewerWebView.getSettings().setDisplayZoomControls(false);

        fileViewerWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                viewerPresenter.onFileLoaded();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

                viewerPresenter.onFileLoadingError();
            }
        });
    }
}
