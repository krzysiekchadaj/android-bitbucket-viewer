package com.kchadaj.bitbucketviewer.screens.repos;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kchadaj.bitbucketviewer.R;

import javax.inject.Inject;

public class ReposAdapter extends RecyclerView.Adapter<ReposViewHolder> {

    private final ReposItemPresenter reposItemPresenter;

    @Inject
    ReposAdapter(ReposItemPresenter reposItemPresenter) {
        this.reposItemPresenter = reposItemPresenter;
    }

    @NonNull
    @Override
    public ReposViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View repoCard = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.card_view_repos, parent, false);
        return new ReposViewHolder(repoCard, reposItemPresenter);
    }

    @Override
    public void onBindViewHolder(@NonNull ReposViewHolder holder, int position) {
        reposItemPresenter.onBindRepoAtPosition(holder, position);
    }

    @Override
    public int getItemCount() {
        return reposItemPresenter.getReposCount();
    }
}
