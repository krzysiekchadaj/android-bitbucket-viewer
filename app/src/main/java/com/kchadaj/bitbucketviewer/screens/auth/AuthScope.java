package com.kchadaj.bitbucketviewer.screens.auth;

import javax.inject.Scope;

@Scope
@interface AuthScope {
    // empty
}
