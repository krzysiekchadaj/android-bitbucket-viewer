package com.kchadaj.bitbucketviewer.screens.navigate.files;

import dagger.Subcomponent;

@FilesScope
@Subcomponent(modules = FilesModule.class)
public interface FilesComponent {

    void inject(FilesFragment filesFragment);
}
