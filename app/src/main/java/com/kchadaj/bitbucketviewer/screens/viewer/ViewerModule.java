package com.kchadaj.bitbucketviewer.screens.viewer;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewerModule {

    private ViewerViewContract viewerViewContract;

    ViewerModule(ViewerViewContract viewerViewContract) {
        this.viewerViewContract = viewerViewContract;
    }

    @ViewerScope
    @Provides
    ViewerViewContract providesViewerViewContract() {
        return viewerViewContract;
    }
}
