package com.kchadaj.bitbucketviewer.screens.repos;

import dagger.Subcomponent;

@ReposScope
@Subcomponent(modules = ReposModule.class)
public interface ReposComponent {

    void inject(ReposFragment reposFragment);
}
