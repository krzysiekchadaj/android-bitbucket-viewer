package com.kchadaj.bitbucketviewer.screens.auth;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.screens.repos.ReposActivity;
import com.kchadaj.bitbucketviewer.utils.Errors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AuthFragment extends Fragment implements AuthViewContract {

    @BindView(R.id.username_edit_text)
    EditText usernameEditText;

    @BindView(R.id.authenticate_button)
    Button authenticateButton;

    private Unbinder unbinder;

    private View view;

    private AuthPresenter authPresenter;

    public AuthFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static AuthFragment newInstance() {
        return new AuthFragment();
    }

    @Inject
    public void setAuthPresenter(AuthPresenter authPresenter) {
        this.authPresenter = authPresenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BitbucketViewer.getComponent()
                .plus(new AuthModule(this))
                .inject(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_auth, container, false);

        unbinder = ButterKnife.bind(this, view);

        usernameEditText.setText(authPresenter.getLastUsername());

        authenticateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = usernameEditText.getText().toString();
                authPresenter.onAuthenticateButtonClick(username);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        FragmentActivity activity = getActivity();
        if (activity != null) {
            Uri uri = activity.getIntent().getData();
            authPresenter.onIncomingAuthUri(uri);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        authenticateButton.setOnClickListener(null);

        unbinder.unbind();

        authPresenter.onDestroyView();
    }

    @Override
    public void requestAuthCode(Uri authCode) {
        startActivity(new Intent(Intent.ACTION_VIEW, authCode));
    }

    @Override
    public void hideSoftKeyboard() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void startReposActivity() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.overridePendingTransition(0, 0);
            startActivity(new Intent(activity, ReposActivity.class));
        }
    }

    @Override
    public void notifyUser(Errors error) {
        Snackbar.make(view, getString(error.getId()), Snackbar.LENGTH_SHORT).show();
    }
}
