package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import dagger.Subcomponent;

@CommitsScope
@Subcomponent(modules = CommitsModule.class)
public interface CommitsComponent {

    void inject(CommitsFragment commitsFragment);
}
