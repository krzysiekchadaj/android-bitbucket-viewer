package com.kchadaj.bitbucketviewer.screens.viewer;

interface ViewerPresenterContract {

    void onViewCreated();

    void onFileLoaded();

    void onFileLoadingError();
}
