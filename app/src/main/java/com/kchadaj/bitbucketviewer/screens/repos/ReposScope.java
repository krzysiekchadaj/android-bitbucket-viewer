package com.kchadaj.bitbucketviewer.screens.repos;

import javax.inject.Scope;

@Scope
@interface ReposScope {
    // empty
}
