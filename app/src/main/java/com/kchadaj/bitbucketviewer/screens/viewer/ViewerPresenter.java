package com.kchadaj.bitbucketviewer.screens.viewer;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.data.Storage;
import com.kchadaj.bitbucketviewer.network.ApiEndpoints;
import com.kchadaj.bitbucketviewer.utils.Errors;

import javax.inject.Inject;

public class ViewerPresenter implements ViewerPresenterContract {

    private final ViewerViewContract view;

    @Inject
    ViewerPresenter(ViewerViewContract view) {
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        view.showProgressBar();
        view.loadFile(constructFileURL());
    }

    @Override
    public void onFileLoaded() {
        view.hideProgressBar();
    }

    @Override
    public void onFileLoadingError() {
        view.hideProgressBar();
        view.notifyUser(Errors.LOADING_FAILED);
    }

    private String constructFileURL() {
        return String.format(ApiEndpoints.BASE_URL + "2.0/repositories/%s/%s/src/%s/%s?access_token=%s",
                BitbucketViewer.getComponent().getStorage().load(Storage.USERNAME),
                BitbucketViewer.getComponent().getStorage().load(Storage.REPO_SLUG),
                BitbucketViewer.getComponent().getStorage().load(Storage.FILES_COMMIT_HASH),
                BitbucketViewer.getComponent().getStorage().load(Storage.FILES_PATH),
                BitbucketViewer.getComponent().getStorage().load(Storage.ACCESS_TOKEN));
    }
}
