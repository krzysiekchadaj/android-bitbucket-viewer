package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.utils.Errors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CommitsFragment extends Fragment implements CommitsViewContract {

    @BindView(R.id.recycler_commits)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private Unbinder unbinder;

    private View view;

    private CommitsPresenter commitsPresenter;

    public CommitsFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static CommitsFragment newInstance() {
        return new CommitsFragment();
    }

    public CommitsPresenter getCommitsPresenter() {
        return commitsPresenter;
    }

    @Inject
    public void setCommitsPresenter(CommitsPresenter commitsPresenter) {
        this.commitsPresenter = commitsPresenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BitbucketViewer.getComponent()
                .plus(new CommitsModule(this))
                .inject(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_commits, container, false);

        unbinder = ButterKnife.bind(this, view);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(commitsPresenter.getCommitsAdapter());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    commitsPresenter.lastItemReached();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        commitsPresenter.requestCommits();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        recyclerView.stopScroll();
        recyclerView.addOnScrollListener(null);

        unbinder.unbind();

        commitsPresenter.onDestroyView();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void notifyUser(Errors error) {
        Snackbar.make(view, getString(error.getId()), Snackbar.LENGTH_SHORT).show();
    }
}
