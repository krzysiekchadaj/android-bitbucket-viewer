package com.kchadaj.bitbucketviewer.screens.repos;

import com.kchadaj.bitbucketviewer.utils.Errors;

interface ReposViewContract {

    void startNavigationActivity();

    void showProgressBar();

    void hideProgressBar();

    void notifyUser(Errors error);
}
