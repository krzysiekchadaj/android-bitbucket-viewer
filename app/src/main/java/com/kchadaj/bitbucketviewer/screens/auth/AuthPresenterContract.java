package com.kchadaj.bitbucketviewer.screens.auth;

import android.net.Uri;

interface AuthPresenterContract {
    void onAuthenticateButtonClick(String username);

    void onIncomingAuthUri(Uri uri);

    String getLastUsername();

    void onDestroyView();
}
