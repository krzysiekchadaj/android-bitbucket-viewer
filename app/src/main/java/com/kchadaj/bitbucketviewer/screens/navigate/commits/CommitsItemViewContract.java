package com.kchadaj.bitbucketviewer.screens.navigate.commits;

public interface CommitsItemViewContract {

    void setCommitHash(String hash);

    void setCommitDate(String date);

    void setCommitMessage(String message);
}
