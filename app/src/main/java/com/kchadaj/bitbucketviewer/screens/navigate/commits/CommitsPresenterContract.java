package com.kchadaj.bitbucketviewer.screens.navigate.commits;

interface CommitsPresenterContract {

    CommitsAdapter getCommitsAdapter();

    void requestCommits();

    void lastItemReached();

    void onDestroyView();
}
