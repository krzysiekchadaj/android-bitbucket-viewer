package com.kchadaj.bitbucketviewer.screens.viewer;

import com.kchadaj.bitbucketviewer.utils.Errors;

public interface ViewerViewContract {

    void loadFile(String fileURL);

    void showProgressBar();

    void hideProgressBar();

    void notifyUser(Errors error);
}
