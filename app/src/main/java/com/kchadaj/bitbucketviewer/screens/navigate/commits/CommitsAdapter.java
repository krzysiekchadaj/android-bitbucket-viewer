package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kchadaj.bitbucketviewer.R;

import javax.inject.Inject;

public class CommitsAdapter extends RecyclerView.Adapter<CommitsViewHolder> {

    private final CommitsItemPresenter commitsItemPresenter;

    @Inject
    CommitsAdapter(CommitsItemPresenter commitsItemPresenter) {
        this.commitsItemPresenter = commitsItemPresenter;
    }

    @NonNull
    @Override
    public CommitsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View repoCard = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.card_view_commits, parent, false);
        return new CommitsViewHolder(repoCard);
    }

    @Override
    public void onBindViewHolder(@NonNull CommitsViewHolder holder, int position) {
        commitsItemPresenter.onBindCommitAtPosition(holder, position);
    }

    @Override
    public int getItemCount() {
        return commitsItemPresenter.getCommitsCount();
    }
}
