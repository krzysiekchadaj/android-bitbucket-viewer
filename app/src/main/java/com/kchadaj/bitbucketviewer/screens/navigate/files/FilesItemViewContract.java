package com.kchadaj.bitbucketviewer.screens.navigate.files;

public interface FilesItemViewContract {

    void setFilePath(String path);
}
