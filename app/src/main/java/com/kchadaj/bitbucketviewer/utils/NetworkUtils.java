package com.kchadaj.bitbucketviewer.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.kchadaj.bitbucketviewer.BitbucketViewer;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public final class NetworkUtils {

    private NetworkUtils() {
        // empty
    }
    
    @NonNull
    public static Boolean isNetworkAvailable() {
        Context context = BitbucketViewer.getComponent().getContext();
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            return (networkInfo != null) && networkInfo.isConnected();
        }
        return false;
    }

    public static <T> void call(Observable<T> endpointMethod, final OnNetworkResultCallback callback) {
        endpointMethod
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<T>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        callback.onStart(d);
                    }

                    @Override
                    public void onNext(T t) {
                        callback.onSuccess(t);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        // empty
                    }
                });
    }
}
