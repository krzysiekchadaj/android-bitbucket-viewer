package com.kchadaj.bitbucketviewer.utils;

import com.kchadaj.bitbucketviewer.R;

public enum Errors {

    NO_ERROR(R.string.error_none),
    AUTHENTICATION_FAILED(R.string.error_auth_failed),
    LOADING_FAILED(R.string.error_loading_failed),
    NO_INTERNET(R.string.error_no_internet),
    NO_INTERNET_LOAD_MORE(R.string.error_no_internet_more),
    USERNAME_INVALID(R.string.error_username_invalid),
    FILE_TYPE_UNKNOWN(R.string.error_file_type_unknown);

    private final int id;

    Errors(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
