package com.kchadaj.bitbucketviewer.screens.repos;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.model.repos.Repo;
import com.kchadaj.bitbucketviewer.utils.CustomMatchers;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ReposScreenInstrumentedTest {

    private static List<Repo> testRepos;

    @Rule
    public ActivityTestRule<ReposActivity> reposActivity =
            new ActivityTestRule<>(ReposActivity.class);

    private ReposAdapter reposAdapter;
    private List<Repo> reposList;

    @BeforeClass
    public static void prepareTestData() {
        testRepos = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            testRepos.add(new Repo("Title " + i, "description " + i, "avatar url " + i));
        }
    }

    @Before
    public void setup() {
        ReposFragment reposFragment = (ReposFragment) reposActivity.getActivity()
                .getSupportFragmentManager()
                .findFragmentById(R.id.repos_container);

        ReposPresenter reposPresenter = reposFragment.getReposPresenter();
        reposAdapter = reposPresenter.getReposAdapter();
        reposList = reposPresenter.getRepositories();
    }

    @Test
    public void reposList_isDisplayed() throws Throwable {
        onView(withId(R.id.repos_container)).check(matches(isDisplayed()));

        reposActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reposList.clear();
                reposList.addAll(testRepos);
                reposAdapter.notifyDataSetChanged();
            }
        });

        Thread.sleep(250);

        for (int i = 0; i < testRepos.size(); i++) {
            Repo repo = testRepos.get(i);
            onView(withId(R.id.recycler_repos))
                    .perform(scrollToPosition(i))
                    .check(matches(CustomMatchers.atPosition(i, hasDescendant(withText(repo.getName())))))
                    .check(matches(CustomMatchers.atPosition(i, hasDescendant(withText(repo.getDescription())))));
        }
    }


}
