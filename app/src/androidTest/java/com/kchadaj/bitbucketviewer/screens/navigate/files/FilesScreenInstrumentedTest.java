package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.model.files.Commit;
import com.kchadaj.bitbucketviewer.model.files.File;
import com.kchadaj.bitbucketviewer.screens.navigate.NavigationActivity;
import com.kchadaj.bitbucketviewer.utils.CustomMatchers;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class FilesScreenInstrumentedTest {

    private static List<File> testFiles;

    @Rule
    public ActivityTestRule<NavigationActivity> navigationActivity =
            new ActivityTestRule<>(NavigationActivity.class);

    private FilesAdapter filesAdapter;
    private List<File> filesList;

    @BeforeClass
    public static void prepareTestData() {
        testFiles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            testFiles.add(new File("path " + i,File.TYPE_FILE, new Commit("hash " + i)));
        }
    }

    @Before
    public void setup() {
        Resources resources = InstrumentationRegistry.getTargetContext().getResources();

        onView(allOf(
                withText(resources.getString(R.string.navigation_source)),
                withId(R.id.largeLabel),
                isDescendantOfA(withId(R.id.navigation)))
        ).perform(click());

        FilesFragment filesFragment = (FilesFragment) navigationActivity.getActivity()
                .getSupportFragmentManager()
                .findFragmentById(R.id.navigation_container);

        FilesPresenter filesPresenter = filesFragment.getFilesPresenter();
        filesAdapter = filesPresenter.getFilesAdapter();
        filesList = filesPresenter.getFiles();
    }

    @Test
    public void filesList_isDisplayed() throws Throwable {
        onView(withId(R.id.navigation_container)).check(matches(isDisplayed()));

        navigationActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                filesList.clear();
                filesList.addAll(testFiles);
                filesAdapter.notifyDataSetChanged();
            }
        });

        Thread.sleep(250);

        for (int i = 0; i < testFiles.size(); i++) {
            File file = testFiles.get(i);
            onView(withId(R.id.recycler_files))
                    .perform(scrollToPosition(i))
                    .check(matches(CustomMatchers.atPosition(i, hasDescendant(withText(file.getPath())))));
        }
    }
}
