package com.kchadaj.bitbucketviewer.screens.viewer;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.kchadaj.bitbucketviewer.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class ViewerScreenInstrumentedTest {

    @Rule
    public ActivityTestRule<ViewerActivity> viewerActivity =
            new ActivityTestRule<>(ViewerActivity.class);

    @Test
    public void viewerScreen_isDisplayed() {
        onView(withId(R.id.file_viewer_web_view)).check(matches(isDisplayed()));
    }
}
