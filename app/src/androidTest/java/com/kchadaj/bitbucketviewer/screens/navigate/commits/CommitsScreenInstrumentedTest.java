package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.kchadaj.bitbucketviewer.R;
import com.kchadaj.bitbucketviewer.model.commits.Commit;
import com.kchadaj.bitbucketviewer.screens.navigate.NavigationActivity;
import com.kchadaj.bitbucketviewer.utils.CustomMatchers;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class CommitsScreenInstrumentedTest {

    private static List<Commit> testCommits;

    @Rule
    public ActivityTestRule<NavigationActivity> navigationActivity =
            new ActivityTestRule<>(NavigationActivity.class);

    private CommitsAdapter commitsAdapter;
    private List<Commit> commitsList;

    @BeforeClass
    public static void prepareTestData() {
        testCommits = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            testCommits.add(new Commit(i + "000000", "date " + i, "message " + i));
        }
    }

    @Before
    public void setup() {
        Resources resources = InstrumentationRegistry.getTargetContext().getResources();

        onView(allOf(
                withText(resources.getString(R.string.navigation_commits)),
                withId(R.id.smallLabel),
                isDescendantOfA(withId(R.id.navigation)))
        ).perform(click());

        CommitsFragment commitsFragment = (CommitsFragment) navigationActivity.getActivity()
                .getSupportFragmentManager()
                .findFragmentById(R.id.navigation_container);

        CommitsPresenter commitsPresenter = commitsFragment.getCommitsPresenter();
        commitsAdapter = commitsPresenter.getCommitsAdapter();
        commitsList = commitsPresenter.getCommits();
    }

    @Test
    public void commitsList_isDisplayed() throws Throwable {
        onView(withId(R.id.navigation_container)).check(matches(isDisplayed()));

        navigationActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                commitsList.clear();
                commitsList.addAll(testCommits);
                commitsAdapter.notifyDataSetChanged();
            }
        });

        Thread.sleep(250);

        for (int i = 0; i < testCommits.size(); i++) {
            Commit commit = testCommits.get(i);
            onView(withId(R.id.recycler_commits))
                    .perform(scrollToPosition(i))
                    .check(matches(CustomMatchers.atPosition(i, hasDescendant(withText(commit.getHash())))))
                    .check(matches(CustomMatchers.atPosition(i, hasDescendant(withText(commit.getDate())))))
                    .check(matches(CustomMatchers.atPosition(i, hasDescendant(withText(commit.getMessage())))));
        }
    }
}
