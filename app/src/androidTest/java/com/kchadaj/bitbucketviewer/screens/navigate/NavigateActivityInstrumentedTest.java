package com.kchadaj.bitbucketviewer.screens.navigate;

import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.kchadaj.bitbucketviewer.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class NavigateActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<NavigationActivity> navigationActivity =
            new ActivityTestRule<>(NavigationActivity.class);

    private Resources resources;

    @Before
    public void setup() {
        resources = InstrumentationRegistry.getTargetContext().getResources();
    }

    @Test
    public void navigateScreen_isBottomNavigationDrawerDisplayed() {
        onView(withId(R.id.navigation)).check(matches(isDisplayed()));

        onSourceItemView().check(matches(isDisplayed()));
        onCommitsItemView().check(matches(isDisplayed()));
    }

    @Test
    public void sourceItemClicked_shouldOpenSourceScreen() {
        onSourceItemView()
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.recycler_files))
                .check(matches(isDisplayed()));
    }

    @Test
    public void commitsItemClicked_shouldOpenCommitsScreen() {
        onCommitsItemView()
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.recycler_commits))
                .check(matches(isDisplayed()));
    }

    private ViewInteraction onSourceItemView() {
        return onView(allOf(
                withText(resources.getString(R.string.navigation_source)),
                withId(R.id.largeLabel), // largeLabel as source menu item is selected by default
                isDescendantOfA(withId(R.id.navigation))));
    }

    private ViewInteraction onCommitsItemView() {
        return onView(allOf(
                withText(resources.getString(R.string.navigation_commits)),
                withId(R.id.smallLabel), // smallLabel as commits menu item is not selected by default
                isDescendantOfA(withId(R.id.navigation))));
    }
}
