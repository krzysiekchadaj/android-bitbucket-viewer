package com.kchadaj.bitbucketviewer.screens.auth;

import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.kchadaj.bitbucketviewer.R;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anyOf;

@RunWith(AndroidJUnit4.class)
public class AuthScreenInstrumentedTest {

    @Rule
    public ActivityTestRule<AuthActivity> authActivity =
            new ActivityTestRule<>(AuthActivity.class);
    private Resources resources;

    @Before
    public void setup() {
        resources = InstrumentationRegistry.getTargetContext().getResources();
    }

    @Test
    public void authScreen_isDisplayed() {
        String editTextHint = resources.getString(R.string.username_edit_text_hint);
        String buttonText = resources.getString(R.string.authenticate_button_text);

        Matcher<View> editText = withId(R.id.username_edit_text);
        Matcher<View> button = withId(R.id.authenticate_button);

        onView(editText).check(matches(isDisplayed()));
        onView(button).check(matches(isDisplayed()));

        onView(editText).perform(clearText());
        onView(editText).check(matches(withHint(editTextHint)));
        onView(button).check(matches(withText(buttonText)));
    }

    @Test
    public void onEmptyUsernameAuth_isSnackBarDisplayed() throws InterruptedException {
        Matcher<View> editText = withId(R.id.username_edit_text);
        Matcher<View> button = withId(R.id.authenticate_button);

        onView(editText).perform(clearText()).perform(closeSoftKeyboard());
        Thread.sleep(250);
        onView(button).perform(click());

        onView(anyOf(
                withText(R.string.error_username_invalid),
                withText(R.string.error_no_internet)
        )).check(matches(isDisplayed()));
    }
}
