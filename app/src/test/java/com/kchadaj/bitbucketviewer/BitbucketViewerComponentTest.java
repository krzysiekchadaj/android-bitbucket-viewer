package com.kchadaj.bitbucketviewer;

import com.kchadaj.bitbucketviewer.data.StorageTest;
import com.kchadaj.bitbucketviewer.screens.auth.AuthComponentTest;
import com.kchadaj.bitbucketviewer.screens.auth.AuthModuleTest;
import com.kchadaj.bitbucketviewer.screens.repos.ReposComponentTest;
import com.kchadaj.bitbucketviewer.screens.repos.ReposModuleTest;

import dagger.Component;

@BitbucketViewerScope
@Component(modules = BitbucketViewerModuleTest.class)
public interface BitbucketViewerComponentTest extends BitbucketViewerComponent {

    AuthComponentTest plus(AuthModuleTest authModuleTest);

    ReposComponentTest plus(ReposModuleTest reposModuleTest);

    void inject(StorageTest storageTest);
}
