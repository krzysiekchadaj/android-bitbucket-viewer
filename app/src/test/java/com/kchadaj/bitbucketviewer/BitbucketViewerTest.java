package com.kchadaj.bitbucketviewer;

import com.kchadaj.bitbucketviewer.modules.ContextModuleTest;

import org.robolectric.RuntimeEnvironment;

public class BitbucketViewerTest extends BitbucketViewer {

    private static BitbucketViewerComponentTest testComponent;

    public static BitbucketViewerComponentTest getComponent() {
        return testComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        testComponent = DaggerBitbucketViewerComponentTest.builder()
                .contextModuleTest(new ContextModuleTest(RuntimeEnvironment.application.getApplicationContext()))
                .build();
    }
}
