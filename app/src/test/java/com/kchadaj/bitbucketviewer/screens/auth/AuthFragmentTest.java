package com.kchadaj.bitbucketviewer.screens.auth;

import android.content.Intent;
import android.net.Uri;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.screens.repos.ReposActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class AuthFragmentTest {

    private AuthFragment authFragment;

    @Before
    public void setup() {
        authFragment = AuthFragment.newInstance();
        SupportFragmentTestUtil.startFragment(authFragment);
    }

    @Test
    public void authButtonClick_shouldAskPresenter() {
        AuthPresenter authPresenter = mock(AuthPresenter.class);
        authFragment.setAuthPresenter(authPresenter);

        authFragment.authenticateButton.performClick();
        verify(authPresenter).onAuthenticateButtonClick(anyString());
    }

    @Test
    public void requestAuthCode_shouldSendIntent() {
        Uri uri = Uri.parse("http://uri.content.not.important.here/");
        authFragment.requestAuthCode(uri);

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW, uri);
        Intent actualIntent = ShadowApplication.getInstance().getNextStartedActivity();
        assertEquals(expectedIntent.getAction(), actualIntent.getAction());
    }

    @Test
    public void startReposActivity_shouldSendIntent() {
        authFragment.startReposActivity();

        Intent expectedIntent = new Intent(authFragment.getActivity(), ReposActivity.class);
        Intent actualIntent = ShadowApplication.getInstance().getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actualIntent.getComponent());
    }
}
