package com.kchadaj.bitbucketviewer.screens.repos;

import dagger.Module;

@Module
public class ReposModuleTest extends ReposModule {

    ReposModuleTest(ReposViewContract reposViewContract) {
        super(reposViewContract);
    }
}
