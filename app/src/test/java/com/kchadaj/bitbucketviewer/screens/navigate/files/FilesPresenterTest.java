package com.kchadaj.bitbucketviewer.screens.navigate.files;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.model.files.File;
import com.kchadaj.bitbucketviewer.network.Api;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class FilesPresenterTest {

    @Mock
    private FilesViewContract view;

    @Mock
    private Api api;

    @Mock
    private List<File> files;

    @Mock
    private FilesAdapter filesAdapter;

    private FilesPresenter reposPresenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        reposPresenter = new FilesPresenter(view, api, files, filesAdapter);
    }

    @Test
    public void requestUserData_shouldAskApi() {
        reposPresenter.requestFiles();
        verify(api).getFiles(
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyInt(),
                any(OnNetworkResultCallback.class));
    }
}
