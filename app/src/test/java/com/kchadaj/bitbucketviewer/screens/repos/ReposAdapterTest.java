package com.kchadaj.bitbucketviewer.screens.repos;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class ReposAdapterTest {

    @Mock
    private ReposItemPresenter reposItemPresenter;

    @Mock
    private ReposViewHolder reposViewHolder;

    private ReposAdapter reposAdapter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        reposAdapter = new ReposAdapter(reposItemPresenter);
    }

    @Test
    public void onBindViewHolder_shouldAskPresenter() {
        int position = 42;
        reposAdapter.onBindViewHolder(reposViewHolder, position);
        verify(reposItemPresenter).onBindRepoAtPosition(reposViewHolder, position);
    }

    @Test
    public void getItemCount_shouldAskPresenter() {
        reposAdapter.getItemCount();
        verify(reposItemPresenter).getReposCount();
    }
}
