package com.kchadaj.bitbucketviewer.screens.navigate.files;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class FilesAdapterTest {

    @Mock
    private FilesItemPresenter filesItemPresenter;

    @Mock
    private FilesViewHolder filesViewHolder;

    private FilesAdapter filesAdapter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        filesAdapter = new FilesAdapter(filesItemPresenter);
    }

    @Test
    public void onBindViewHolder_shouldAskPresenter() {
        int position = 42;
        filesAdapter.onBindViewHolder(filesViewHolder, position);
        verify(filesItemPresenter).onBindFileAtPosition(filesViewHolder, position);
    }

    @Test
    public void getItemCount_shouldAskPresenter() {
        filesAdapter.getItemCount();
        verify(filesItemPresenter).getFilesCount();
    }
}
