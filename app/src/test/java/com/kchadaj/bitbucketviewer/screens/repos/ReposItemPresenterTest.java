package com.kchadaj.bitbucketviewer.screens.repos;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.model.repos.Repo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class ReposItemPresenterTest {

    @Mock
    private ReposViewContract reposViewContract;

    private List<Repo> repositories = new ArrayList<>(0);
    private List<Repo> spyRepositories = Mockito.spy(repositories);

    private ReposItemPresenter reposItemPresenter;

    private Repo repo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        repo = new Repo("Repo name", "Repo description", "Repo avatar URL");

        spyRepositories.clear();
        spyRepositories.add(repo);

        reposItemPresenter = new ReposItemPresenter(spyRepositories, reposViewContract);
    }

    @Test
    public void getReposCount_shouldReturnRepositoriesSize() {
        reposItemPresenter.getReposCount();
        verify(spyRepositories).size();
    }

    @Test
    public void onBindRepoAtPosition_shouldSetItemView() {
        ReposItemViewContract itemView = mock(ReposItemViewContract.class);
        reposItemPresenter.onBindRepoAtPosition(itemView, 0);

        verify(itemView).setRepoTitle(repo.getName());
        verify(itemView).setRepoDescription(repo.getDescription());
        verify(itemView).setRepoAvatar(repo.getLinks().getAvatar().getHref());
    }

    @Test
    public void onRepoClickAtPosition_shouldAskView() {
        reposItemPresenter.onRepoClickAtPosition(0);
        verify(reposViewContract).startNavigationActivity();
    }
}
