package com.kchadaj.bitbucketviewer.screens.navigate.files;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.model.files.Commit;
import com.kchadaj.bitbucketviewer.model.files.File;
import com.kchadaj.bitbucketviewer.utils.Errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class FilesItemPresenterTest {

    @Mock
    private FilesViewContract filesViewContract;

    private List<File> files = new ArrayList<>(0);
    private List<File> spyFiles = Mockito.spy(files);

    private FilesItemPresenter filesItemPresenter;

    private File file;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        file = new File("some path", File.TYPE_FILE, new Commit("some commit"));

        spyFiles.clear();
        spyFiles.add(file);
        spyFiles.add(new File("dir file path", File.TYPE_DIRECTORY, new Commit("dir file commit")));
        spyFiles.add(new File("file file path", File.TYPE_FILE, new Commit("file file commit")));
        spyFiles.add(new File("other file path", "some other type", new Commit("other file commit")));

        filesItemPresenter = new FilesItemPresenter(spyFiles, filesViewContract);
    }

    @Test
    public void getFilesCount_shouldReturnFilesSize() {
        filesItemPresenter.getFilesCount();
        verify(spyFiles).size();
    }

    @Test
    public void onBindFileAtPosition_shouldSetItemView() {
        FilesItemViewContract itemView = mock(FilesItemViewContract.class);
        filesItemPresenter.onBindFileAtPosition(itemView, 0);
        verify(itemView).setFilePath(file.getPath());
    }

    @Test
    public void onFileClickAtPosition_shouldAskView() {
        filesItemPresenter.onFileClickAtPosition(1);
        verify(filesViewContract).startNewFilesScreen();

        filesItemPresenter.onFileClickAtPosition(2);
        verify(filesViewContract).startFileViewerScreen();

        filesItemPresenter.onFileClickAtPosition(3);
        verify(filesViewContract).notifyUser(any(Errors.class));
    }
}
