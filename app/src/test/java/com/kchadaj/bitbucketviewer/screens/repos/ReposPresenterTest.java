package com.kchadaj.bitbucketviewer.screens.repos;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.model.repos.Repo;
import com.kchadaj.bitbucketviewer.network.Api;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class ReposPresenterTest {

    @Mock
    private ReposViewContract view;

    @Mock
    private Api api;

    @Mock
    private List<Repo> repositories;

    @Mock
    private ReposAdapter reposAdapter;

    private ReposPresenter reposPresenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        reposPresenter = new ReposPresenter(view, api, repositories, reposAdapter);
    }

    @Test
    public void requestUserData_shouldAskApi() {
        reposPresenter.requestUserRepos();
        verify(api).getRepos(anyString(), anyString(), anyInt(), any(OnNetworkResultCallback.class));
    }
}
