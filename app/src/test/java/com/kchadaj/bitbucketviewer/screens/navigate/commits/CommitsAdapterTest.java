package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class CommitsAdapterTest {

    @Mock
    private CommitsItemPresenter commitsItemPresenter;

    @Mock
    private CommitsViewHolder commitsViewHolder;

    private CommitsAdapter commitsAdapter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        commitsAdapter = new CommitsAdapter(commitsItemPresenter);
    }

    @Test
    public void onBindViewHolder_shouldAskPresenter() {
        int position = 42;
        commitsAdapter.onBindViewHolder(commitsViewHolder, position);
        verify(commitsItemPresenter).onBindCommitAtPosition(commitsViewHolder, position);
    }

    @Test
    public void getItemCount_shouldAskPresenter() {
        commitsAdapter.getItemCount();
        verify(commitsItemPresenter).getCommitsCount();
    }
}
