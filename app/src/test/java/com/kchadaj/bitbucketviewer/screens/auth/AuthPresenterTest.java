package com.kchadaj.bitbucketviewer.screens.auth;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.data.Authentication;
import com.kchadaj.bitbucketviewer.utils.Errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class AuthPresenterTest {

    @Inject
    AuthPresenter authPresenter;

    @Mock
    private AuthViewContract view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        BitbucketViewerTest.getComponent()
                .plus(new AuthModuleTest(view))
                .inject(this);
    }

    @Test
    public void onAuthenticateButtonClick_shouldCallViewAndHideKeyboard() {
        authPresenter.onAuthenticateButtonClick("some_user");
        verify(view).hideSoftKeyboard();
        verify(view).requestAuthCode(Authentication.authCodeUri);
    }

    @Test
    public void onAuthenticateButtonClickWithNull_shouldNotCallView() {
        authPresenter.onAuthenticateButtonClick(null);
        verify(view).notifyUser(Errors.USERNAME_INVALID);
        verify(view, never()).requestAuthCode(Authentication.authCodeUri);
    }

    @Test
    public void onAuthenticateButtonClickWithEmpty_shouldNotCallView() {
        authPresenter.onAuthenticateButtonClick("");
        verify(view).notifyUser(Errors.USERNAME_INVALID);
        verify(view, never()).requestAuthCode(Authentication.authCodeUri);
    }

    @Test
    public void onAuthenticateButtonClick_shouldUpdateLastUsername() {
        String expectedUsername = "valid_username_for_update_last_username_test";
        authPresenter.onAuthenticateButtonClick(expectedUsername);
        String actualUsername = authPresenter.getLastUsername();
        assertEquals(expectedUsername, actualUsername);
    }
}
