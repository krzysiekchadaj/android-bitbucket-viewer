package com.kchadaj.bitbucketviewer.screens.auth;

import dagger.Subcomponent;

@AuthScope
@Subcomponent(modules = AuthModuleTest.class)
public interface AuthComponentTest {
    void inject(AuthPresenterTest authPresenterTest);
}
