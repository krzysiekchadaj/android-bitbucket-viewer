package com.kchadaj.bitbucketviewer.screens.repos;

import android.content.Intent;
import android.view.View;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.screens.navigate.NavigationActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class ReposFragmentTest {

    private ReposFragment reposFragment;

    @Before
    public void setup() {
        reposFragment = ReposFragment.newInstance();
        SupportFragmentTestUtil.startFragment(reposFragment);
    }

    @Test
    public void startNavigationActivity_shouldSendIntent() {
        reposFragment.startNavigationActivity();

        Intent expectedIntent = new Intent(reposFragment.getActivity(), NavigationActivity.class);
        Intent actualIntent = ShadowApplication.getInstance().getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actualIntent.getComponent());
    }

    @Test
    public void showProgressBar_shouldShowIt() {
        reposFragment.showProgressBar();
        int visibility = reposFragment.progressBar.getVisibility();
        assertTrue(visibility == View.VISIBLE);
    }

    @Test
    public void hideProgressBar_shouldHideIt() {
        reposFragment.hideProgressBar();
        int visibility = reposFragment.progressBar.getVisibility();
        assertTrue(visibility == View.INVISIBLE || visibility == View.GONE);
    }
}
