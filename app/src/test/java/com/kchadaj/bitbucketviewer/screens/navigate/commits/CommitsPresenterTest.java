package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.model.commits.Commit;
import com.kchadaj.bitbucketviewer.network.Api;
import com.kchadaj.bitbucketviewer.network.OnNetworkResultCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class CommitsPresenterTest {

    @Mock
    private CommitsViewContract view;

    @Mock
    private Api api;

    @Mock
    private List<Commit> commits;

    @Mock
    private CommitsAdapter commitsAdapter;

    private CommitsPresenter commitsPresenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        commitsPresenter = new CommitsPresenter(view, api, commits, commitsAdapter);
    }

    @Test
    public void requestCommits_shouldAskApi() {
        commitsPresenter.requestCommits();
        verify(api).getCommits(anyString(), anyString(), anyString(), anyInt(), any(OnNetworkResultCallback.class));
    }
}
