package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import android.view.View;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class CommitsFragmentTest {

    private CommitsFragment commitsFragment;

    @Before
    public void setup() {
        commitsFragment = CommitsFragment.newInstance();
        SupportFragmentTestUtil.startFragment(commitsFragment);
    }

    @Test
    public void showProgressBar_shouldShowIt() {
        commitsFragment.showProgressBar();
        int visibility = commitsFragment.progressBar.getVisibility();
        assertTrue(visibility == View.VISIBLE);
    }

    @Test
    public void hideProgressBar_shouldHideIt() {
        commitsFragment.hideProgressBar();
        int visibility = commitsFragment.progressBar.getVisibility();
        assertTrue(visibility == View.INVISIBLE || visibility == View.GONE);
    }
}
