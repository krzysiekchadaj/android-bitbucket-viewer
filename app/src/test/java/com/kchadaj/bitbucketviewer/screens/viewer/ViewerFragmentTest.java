package com.kchadaj.bitbucketviewer.screens.viewer;

import android.view.View;
import android.webkit.WebView;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class ViewerFragmentTest {

    @Mock
    private WebView webView;

    private ViewerFragment viewerFragment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        viewerFragment = ViewerFragment.newInstance();
        SupportFragmentTestUtil.startFragment(viewerFragment);

        viewerFragment.fileViewerWebView = webView;
    }

    @Test
    public void loadFile_shouldAskWebView() {
        String fileUrl = "http://www.example.com";
        viewerFragment.loadFile(fileUrl);
        verify(webView).loadUrl(fileUrl);
    }

    @Test
    public void showProgressBar_shouldShowIt() {
        viewerFragment.showProgressBar();
        int visibility = viewerFragment.progressBar.getVisibility();
        assertTrue(visibility == View.VISIBLE);
    }

    @Test
    public void hideProgressBar_shouldHideIt() {
        viewerFragment.hideProgressBar();
        int visibility = viewerFragment.progressBar.getVisibility();
        assertTrue(visibility == View.INVISIBLE || visibility == View.GONE);
    }
}