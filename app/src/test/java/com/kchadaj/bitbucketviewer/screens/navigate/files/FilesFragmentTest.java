package com.kchadaj.bitbucketviewer.screens.navigate.files;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.screens.navigate.NavigationActivity;
import com.kchadaj.bitbucketviewer.screens.viewer.ViewerActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class FilesFragmentTest {

    private FilesFragment filesFragment;

    @Before
    public void setup() {
        filesFragment = FilesFragment.newInstance();
        startFragment(filesFragment);
    }

    @Test
    public void startFileViewerScreen_shouldSendIntent() {
        filesFragment.startFileViewerScreen();

        Intent expectedIntent = new Intent(filesFragment.getActivity(), ViewerActivity.class);
        Intent actualIntent = ShadowApplication.getInstance().getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actualIntent.getComponent());
    }

    @Test
    public void showProgressBar_shouldShowIt() {
        filesFragment.showProgressBar();
        int visibility = filesFragment.progressBar.getVisibility();
        assertTrue(visibility == View.VISIBLE);
    }

    @Test
    public void hideProgressBar_shouldHideIt() {
        filesFragment.hideProgressBar();
        int visibility = filesFragment.progressBar.getVisibility();
        assertTrue(visibility == View.INVISIBLE || visibility == View.GONE);
    }

    private void startFragment(Fragment fragment) {
        NavigationActivity navigationActivity = Robolectric.buildActivity(NavigationActivity.class)
                .create()
                .start()
                .resume()
                .get();

        navigationActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(fragment, null)
                .commit();
    }
}
