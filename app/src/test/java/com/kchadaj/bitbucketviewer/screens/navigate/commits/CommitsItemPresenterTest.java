package com.kchadaj.bitbucketviewer.screens.navigate.commits;

import com.kchadaj.bitbucketviewer.model.commits.Commit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CommitsItemPresenterTest {

    private List<Commit> commits = new ArrayList<>(0);
    private List<Commit> spyCommits = Mockito.spy(commits);

    private CommitsItemPresenter commitsItemPresenter;

    @Before
    public void setup() {
        commitsItemPresenter = new CommitsItemPresenter(spyCommits);
    }

    @Test
    public void getCommitsCount_shouldReturnCommitsSize() {
        commitsItemPresenter.getCommitsCount();
        verify(spyCommits).size();
    }

    @Test
    public void onBindCommitAtPosition_shouldSetItemView() {
        Commit commit = new Commit(
                "953368b38e6f1cd1897e8a5660daa089b8e7bfbc",
                "2018-06-04T12:27:02+00:00",
                "A particular commit message\n");

        spyCommits.clear();
        spyCommits.add(commit);

        CommitsViewHolder itemView = mock(CommitsViewHolder.class);
        commitsItemPresenter.onBindCommitAtPosition(itemView, 0);

        verify(itemView).setCommitHash("953368b");
        verify(itemView).setCommitDate("14:27 04-06-2018");
        verify(itemView).setCommitMessage("A particular commit message");
    }
}
