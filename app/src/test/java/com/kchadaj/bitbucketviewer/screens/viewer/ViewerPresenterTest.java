package com.kchadaj.bitbucketviewer.screens.viewer;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;
import com.kchadaj.bitbucketviewer.utils.Errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class ViewerPresenterTest {

    @Mock
    private ViewerViewContract view;

    private ViewerPresenter viewerPresenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        viewerPresenter = new ViewerPresenter(view);
    }

    @Test
    public void onViewCreated_shouldAskView() {
        viewerPresenter.onViewCreated();
        verify(view).showProgressBar();
        verify(view).loadFile(anyString());
    }

    @Test
    public void onFileLoaded_shouldHideProgressBar() {
        viewerPresenter.onFileLoaded();
        verify(view).hideProgressBar();
    }

    @Test
    public void onFileLoadingError_shouldNotifyUser() {
        viewerPresenter.onFileLoadingError();
        verify(view).hideProgressBar();
        verify(view).notifyUser(Errors.LOADING_FAILED);
    }
}
