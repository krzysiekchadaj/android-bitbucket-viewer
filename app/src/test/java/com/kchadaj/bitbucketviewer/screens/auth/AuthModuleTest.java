package com.kchadaj.bitbucketviewer.screens.auth;

import dagger.Module;

@Module
public class AuthModuleTest extends AuthModule {

    AuthModuleTest(AuthViewContract authViewContract) {
        super(authViewContract);
    }
}
