package com.kchadaj.bitbucketviewer.screens.repos;

import dagger.Subcomponent;

@ReposScope
@Subcomponent(modules = ReposModuleTest.class)
public interface ReposComponentTest {
    // empty
}
