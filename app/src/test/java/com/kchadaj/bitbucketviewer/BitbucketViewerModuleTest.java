package com.kchadaj.bitbucketviewer;

import com.kchadaj.bitbucketviewer.modules.ApiModuleTest;
import com.kchadaj.bitbucketviewer.modules.ContextModuleTest;
import com.kchadaj.bitbucketviewer.modules.ServiceModuleTest;
import com.kchadaj.bitbucketviewer.modules.StorageModuleTest;
import com.kchadaj.bitbucketviewer.screens.auth.AuthComponentTest;
import com.kchadaj.bitbucketviewer.screens.repos.ReposComponentTest;

import dagger.Module;

@Module(includes = {
        ApiModuleTest.class,
        ServiceModuleTest.class,
        StorageModuleTest.class,
        ContextModuleTest.class
}, subcomponents = {
        AuthComponentTest.class,
        ReposComponentTest.class})
class BitbucketViewerModuleTest {
    // empty
}
