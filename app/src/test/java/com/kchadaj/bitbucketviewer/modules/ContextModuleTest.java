package com.kchadaj.bitbucketviewer.modules;

import android.content.Context;

import dagger.Module;

@Module
public class ContextModuleTest extends ContextModule {

    public ContextModuleTest(Context context) {
        super(context);
    }
}
