package com.kchadaj.bitbucketviewer.modules;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;
import com.kchadaj.bitbucketviewer.network.Service;

import org.mockito.Mockito;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModuleTest {

    @Provides
    @BitbucketViewerScope
    Service provideService() {
        return Mockito.mock(Service.class);
    }
}
