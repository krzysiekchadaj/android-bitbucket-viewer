package com.kchadaj.bitbucketviewer.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;
import com.kchadaj.bitbucketviewer.data.Storage;

import org.robolectric.RuntimeEnvironment;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModuleTest {

    private static String sharedPreferencesFileKeyTest = Storage.class.getName() + ".file.test";

    @Provides
    @BitbucketViewerScope
    public SharedPreferences providesSharedPreferences() {
        return RuntimeEnvironment.application.getSharedPreferences(sharedPreferencesFileKeyTest, Context.MODE_PRIVATE);
    }
}
