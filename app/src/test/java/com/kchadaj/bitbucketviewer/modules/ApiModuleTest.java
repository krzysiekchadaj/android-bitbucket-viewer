package com.kchadaj.bitbucketviewer.modules;

import com.kchadaj.bitbucketviewer.BitbucketViewerScope;
import com.kchadaj.bitbucketviewer.network.Api;

import org.mockito.Mockito;

import dagger.Module;
import dagger.Provides;

@Module
public class ApiModuleTest {

    @Provides
    @BitbucketViewerScope
    Api provideApi() {
        return Mockito.mock(Api.class);
    }
}
