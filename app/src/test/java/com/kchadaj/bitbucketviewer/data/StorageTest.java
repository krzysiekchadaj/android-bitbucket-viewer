package com.kchadaj.bitbucketviewer.data;

import com.kchadaj.bitbucketviewer.BitbucketViewerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(application = BitbucketViewerTest.class)
public class StorageTest {

    @Inject
    Storage storage;

    @Before
    public void setup() {
        BitbucketViewerTest.getComponent().inject(this);
    }

    @Test
    public void saveAndLoad_shouldPreserveValue() {
        String key = "test";
        String expected = "test_value";
        storage.save(key, expected);
        String actual = storage.load(key);
        assertEquals(expected, actual);
    }

    @Test
    public void saveAndLoadState_shouldPreserveValue() {
        String key = "test_state";
        storage.saveState(key, true);
        assertTrue(storage.loadState(key));
    }
}
